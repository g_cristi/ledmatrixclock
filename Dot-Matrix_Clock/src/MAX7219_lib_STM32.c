/*
 * MAX7219_lib_STM32.c
 *
 *  Created on: Jan 22, 2017
 *      Author: Cristian Gantner
 */

#include "stm32f10x.h"
#include "MAX7219_lib_STM32.h"


void SPI_init(void){
	//initialize spi for sending data to the led-matrix display
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

	GPIO_InitStructure.GPIO_Pin = MAX7219_SPI_clk | MAX7219_SPI_data; //SCK, MOSI
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(MAX7219_SPI_port, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = MAX7219_SPI_cs;			//CS
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(MAX7219_SPI_cs_port, &GPIO_InitStructure);

	GPIO_WriteBit(MAX7219_SPI_cs_port, MAX7219_SPI_cs, SET);

	SPI_InitStruct.SPI_Direction=SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode=SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize=SPI_DataSize_16b;
	SPI_InitStruct.SPI_CPOL=SPI_CPOL_Low;
	SPI_InitStruct.SPI_CPHA=SPI_CPHA_1Edge;
	SPI_InitStruct.SPI_NSS=SPI_NSS_Soft;
	SPI_InitStruct.SPI_BaudRatePrescaler=SPI_BaudRatePrescaler_16;
	SPI_InitStruct.SPI_FirstBit=SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_CRCPolynomial = 0;
	SPI_Init(MAX7219_SPI, &SPI_InitStruct);
	SPI_Cmd(MAX7219_SPI, ENABLE);
}

void MAX7219_init(void){
	char i,k;
	uint16_t data;
	//setting led intensity
	data = MAX7219_intensity;
	data = data<<8|0x00;
	MAX7219_CS_RESET;
	for(k=0; k<MAX7219_count; k++){
		SPI_I2S_SendData(MAX7219_SPI, data);
		while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
		while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
	}
	MAX7219_CS_SET;

	//setting number of columns used
	data = MAX7219_scanLimit;
	data = data<<8|0x07;
	MAX7219_CS_RESET;
	for(k=0; k<MAX7219_count; k++){
		SPI_I2S_SendData(MAX7219_SPI, data);
		while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
		while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
	}
	MAX7219_CS_SET;

	//set chip to normal operation
	data = MAX7219_shutdown;
	data = data<<8|0x01;
	MAX7219_CS_RESET;
	MAX7219_CS_RESET;
	for(k=0; k<MAX7219_count; k++){
		SPI_I2S_SendData(MAX7219_SPI, data);
		while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
		while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
	}
	MAX7219_CS_SET;

	//set decoding mode to none
	data = MAX7219_decodeMode;
	data = data<<8|0x00;
	MAX7219_CS_RESET;
	MAX7219_CS_RESET;
	for(k=0; k<MAX7219_count; k++){
		SPI_I2S_SendData(MAX7219_SPI, data);
		while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
		while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
	}
	MAX7219_CS_SET;

	//clear digit data
	for(i=1; i<9; i++){
		MAX7219_CS_RESET;
		for(k=0; k<MAX7219_count; k++){
			data = i;
			data = data<<8|0x00;
			SPI_I2S_SendData(MAX7219_SPI, data);
			while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
			while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
		}
		MAX7219_CS_SET;
	}
}

void MAX7219_displayInit(void){
	SPI_init();
	MAX7219_init();
}

void clearBuffer(void){
	//uint16_t data;
	uint8_t i;
	//clear buffer
	for(i=0; i<height*MAX7219_count; i++){
		buffer[i]=0x00;
	}
}

void updateDisplay(void){
	/* updates the whole display
	 * takes in account number of MAX7219 in series
	 *
	 *
	*/

	char i, k;
	uint16_t data;

	for(i=8; i>0; i--){
		MAX7219_CS_RESET;
		for(k=MAX7219_count; k>0; k--){
			data = i;
			data = data<<8|buffer[(i+8*k-9)];
			SPI_I2S_SendData(MAX7219_SPI, data);
			while (!SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_TXE));
			while (SPI_I2S_GetFlagStatus(MAX7219_SPI, SPI_I2S_FLAG_BSY));
		}
		MAX7219_CS_SET;
	}
}
