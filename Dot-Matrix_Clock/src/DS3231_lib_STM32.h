/*
 * DS3231_lib_STM32.h
 *
 *  Created on: Jan 22, 2017
 *      Author: Cristian Gantner
 *
 *      This library is written for the STM32F103 and uses the I2C1
 *      PB6 - SCL, PB7 - SDA
 *
 */

#ifndef DS3231_LIB_STM32_H_
#define DS3231_LIB_STM32_H_

#define DS3231_I2C_port GPIOB
#define DS3231_I2C		I2C1
#define DS3231_I2C_SCL 	GPIO_Pin_6
#define DS3231_I2C_SDA 	GPIO_Pin_7
//!!!change RCC clocks in I2C_init according to port and i2c used

#define DS3231_addressWrite 0xD0
#define DS3231_addressRead 0xD1

#define DS3231_secondsReg 0x00
#define DS3231_minutesReg 0x01
#define DS3231_hourReg 0x02
#define DS3231_dayReg 0x03
#define DS3231_dateReg 0x04
#define DS3231_monthReg 0x05
#define DS3231_yearReg 0x06
#define DS3231_controlReg 0x0E
#define DS3231_statusReg 0x0F
#define DS3231_agingReg 0x10
#define DS3231_TempReg 0x11
#define DS3231_tempReg 0x12

struct DS3231_reg_t{
	uint8_t Second;
	uint8_t second;
	uint8_t Hour;
	uint8_t hour;
	uint8_t Min;
	uint8_t min;
	uint8_t Date;
	uint8_t date;
	uint8_t Month;
	uint8_t month;
	uint8_t Year;
	uint8_t year;
	int8_t TempI;	//integer part of the temperature
	uint8_t temp;	//fractional aprt in 0.25*C steps
};

struct DS3231_reg_t DS3231_reg;

void DS3231_init();
void DS3231_read();
void DS3231_update();

#endif /* DS3231_LIB_STM32_H_ */
