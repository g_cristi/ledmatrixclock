/*
 * MAX7219_lib_STM32.h
 *
 *  Created on: Jan 22, 2017
 *      Author: Cristian Gantner
 *
 *      This library is written for the STM32F103 and uses the SPI1
 *      PA7 - MOSI, PA5 - CLOCK, PA4 - CS
 *
 */

//defines//
#define height 8		//height of display is 1 module, so 8 leds
#define width 40		//width of display is 5 modules, so 40 leds

#define MAX7219_count 5	//number of MAX7219 chained in series

#define MAX7219_SPI 			SPI2
#define MAX7219_SPI_port 		GPIOB
#define MAX7219_SPI_clk 	GPIO_Pin_13
#define MAX7219_SPI_data 	GPIO_Pin_15
#define MAX7219_SPI_cs_port 	GPIOB
#define MAX7219_SPI_cs 		GPIO_Pin_12
//!!!change RCC clocks in SPI_init according to port and spi used

#define MAX7219_noop         0x00
#define MAX7219_digit0       0x01
#define MAX7219_digit1       0x02
#define MAX7219_digit2       0x03
#define MAX7219_digit3       0x04
#define MAX7219_digit4       0x05
#define MAX7219_digit5       0x06
#define MAX7219_digit6       0x07
#define MAX7219_digit7       0x08
#define MAX7219_decodeMode   0x09
#define MAX7219_intensity    0x0A
#define MAX7219_scanLimit    0x0B
#define MAX7219_shutdown     0x0C
#define MAX7219_displayTest  0x0F

#define MAX7219_CS_SET			GPIO_SetBits(MAX7219_SPI_cs_port, MAX7219_SPI_cs)
#define MAX7219_CS_RESET		GPIO_ResetBits(MAX7219_SPI_cs_port, MAX7219_SPI_cs)

#ifndef MAX7219_LIB_STM32_H_
#define MAX7219_LIB_STM32_H_

//variables//
uint8_t buffer[height*MAX7219_count];		//buffer for 40 bytes, every column is 1byte.

//functions//
void SPI_init();

void MAX7219_init();

extern void MAX7219_displayInit();

extern void clearBuffer();

extern void updateDisplay();

#endif /* MAX7219_LIB_STM32_H_ */
