/*
 * DS3231_lib_STM32.c
 *
 *  Created on: Jan 22, 2017
 *      Author: Cristian Gantner
 */


#include "stm32f10x.h"
#include "DS3231_lib_STM32.h"
#include <stdio.h>




void I2C_init(){
	/*
	 * setup I2C communication
	 */

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE);		//enable GPIOB and AF clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);							//enable I2C clock

	GPIO_InitTypeDef GPIO_InitStructure;
	I2C_InitTypeDef I2C_InitStructure;

	/* Configure the GPIOs used to drive I2C*/
	GPIO_InitStructure.GPIO_Pin = DS3231_I2C_SDA|DS3231_I2C_SCL;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(DS3231_I2C_port, &GPIO_InitStructure);

	I2C_DeInit(DS3231_I2C); //reset I2C registers to default value
	I2C_InitStructure.I2C_Ack=I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress=I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed=100000;
	I2C_InitStructure.I2C_DutyCycle=I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Mode=I2C_Mode_I2C;
	I2C_InitStructure.I2C_OwnAddress1=0;
	I2C_Init(DS3231_I2C, &I2C_InitStructure);
	I2C_Cmd(DS3231_I2C, ENABLE);
}

void I2C_Start(uint8_t RW ){
	// wait until I2C1 is not busy any more
	while(I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_BUSY));
	//generate I2C Start signal
	I2C_GenerateSTART(DS3231_I2C, ENABLE);
	//wait until master mode is selected
	while (!I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_MODE_SELECT));
	//	while (!I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_SB));
	//send 7 bit address
	if(RW == 0){//transmit write
	I2C_Send7bitAddress(DS3231_I2C, DS3231_addressWrite, I2C_Direction_Transmitter);
	while (!I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	//while (!I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_ADDR));
	}else if (RW == 1) {//transmit read
		I2C_Send7bitAddress(DS3231_I2C, DS3231_addressRead, I2C_Direction_Receiver);
		while (!I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
		//while (!I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_ADDR));
		//while (!I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_ADDR));
	}else {
		return;
	}
}

void I2C_Send(uint8_t data){
	//send byte to OLED
	I2C_SendData(DS3231_I2C, data);
	//wait until byte is transmitted
	while(!I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	//while(!I2C_GetFlagStatus(DS3231_I2C, I2C_FLAG_BTF));
}

int I2C_ReceiveACK(){
	//returns value received from I2C deviece
	// enable acknowledge of received data
	I2C_AcknowledgeConfig(DS3231_I2C, ENABLE);
	// wait until one byte has been received
	while( !I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(DS3231_I2C);
	return data;
}

int I2C_ReceiveNACK(){
	//returns value received from I2C deviece
	// disable acknowledge of received data
	// nack also generates stop condition after last byte received
	// see reference manual for more info
	I2C_AcknowledgeConfig(DS3231_I2C, DISABLE);
	I2C_GenerateSTOP(DS3231_I2C, ENABLE);
	// wait until one byte has been received
	while( !I2C_CheckEvent(DS3231_I2C, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(DS3231_I2C);
	return data;

}

void I2C_Stop(void){
	//generate I2C stop signal
	I2C_GenerateSTOP(DS3231_I2C, ENABLE);
}

void DS3231_update(void){
	/*
	 * writes the current time to the registers of the DS3231
	 */
	uint8_t i=0;
	//start communcation and point to minutes register
	I2C_Start(0);
	I2C_Send(DS3231_minutesReg);

	//send minutes set by user
	i=DS3231_reg.Min;
	i = (i<<4)| DS3231_reg.min;
	I2C_Send(i);
	//send hour set by user
	i=DS3231_reg.Hour;
	i = (i<<4) | DS3231_reg.hour;
	I2C_Send(i);
	//send date set by user

	//send month set by user

	//send year set by user
	I2C_Stop();
}

void DS3231_read(void){
	/*
	 * reads the values from the DS3231 and saves them in global variables
	 * time, date, temp
	 */
	uint8_t i;
	//set address pointer of DS3231 to 0x00
	I2C_Start(0);
	I2C_Send(DS3231_secondsReg);
	I2C_Stop();

	I2C_Start(1);
	//read seconds byte
	i = I2C_ReceiveACK();
	DS3231_reg.Second = i>>4;
	DS3231_reg.second = i&0x0F;
	//read minutes byte
	i = I2C_ReceiveACK();
	DS3231_reg.Min = i>>4;
	DS3231_reg.min = i&0x0F;
	//read hours byte
	i = I2C_ReceiveACK();
	DS3231_reg.Hour = (i>>4)&0x03;
	DS3231_reg.hour = i&0x0F;
	//read date
	i = I2C_ReceiveACK();
	DS3231_reg.Date = (i>>4);
	DS3231_reg.date = i&0x0F;
	//read month
	i = I2C_ReceiveACK();
	DS3231_reg.Month = (i>>4)&0x01;
	DS3231_reg.month = i&0x0F;
	I2C_Stop();
	//read year
	i = I2C_ReceiveNACK();
	DS3231_reg.Year = (i>>4);
	DS3231_reg.year = i&0x0F;

	//send start and point to register 0x11, afterwards restart and read MSB of temperature
	I2C_Start(0);
	I2C_Send(DS3231_TempReg);
	I2C_Stop();

	I2C_Start(1);
	//read temp
	i = I2C_ReceiveNACK();
	DS3231_reg.TempI = i;
	I2C_Stop();


}

void DS3231_init(void){
	/*
	 *
	 *writes the current time to the registers of the DS3231
	 */
	I2C_init();
	DS3231_read();
}
